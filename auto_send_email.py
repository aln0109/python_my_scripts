"""
参考链接:
1. https://blog.csdn.net/u010595191/article/details/80104947
2. https://blog.csdn.net/bwz771411855/article/details/107968495
"""


import sys
import imp
import time
import warnings
import pythoncom
import numpy as np
import datetime as dt
import dateutil.parser
import win32com.client as win32

from win32com.client.gencache import EnsureDispatch as Dispatch


def sendEmail(receivers: list, cc: list, sub: str, body: str):
    print('sendEmail begin...')
    imp.reload(sys)
    warnings.filterwarnings('ignore')
    pythoncom.CoInitialize()
    
    outlook = win32.Dispatch('outlook.application')
    print('get outlook successfully...')
    
    mail = outlook.CreateItem(0)
    print('mail item type ==', type(mail))
    
    mail.To = receivers[0]
    print('get mail successfully...')
    
    if(0 != len(cc)):
        print('cc is not empty...')
        mail.CC = cc[0]
    
    mail.Subject = sub
    mail.Body = body
    # 添加附件
    # mail.Attachments.Add(r'附件路径.txt')
    # print('Attachments add successfully...')
    
    mail.Send()
    print('sendEmail end...')


# 邮件接收时间格式转换(pywintyptes.datetime -> datetime.datetime)
def date_format_trains(trains_date):
    trains_after = dateutil.parser.parse(trains_date)
    return trains_after.year, trains_after.month, trains_after.day

# 在指定时间点后,跳出整个脚本
def app_timer_out(hour: int, minute: int):
    if(dt.datetime.now().hour>hour and dt.datetime.now().minute>minute):
        print('app_timer_out dt.datetime.now().hour ==', dt.datetime.now().hour)
        print('app_timer_out dt.datetime.now().minute ==', dt.datetime.now().minute)
        return -1


# 筛选发送条件
def select_send_condition(receivers: list, cc: list, sub: str, body: str):
    print('select_send_condition begin...')
    # 初始化 ol 对象(固定写法)
    outlook = Dispatch('Outlook.Application').GetNamespace('MAPI')
    accounts = outlook.Folders
    
    # 邮件发送次数统计
    send_count = 0
    
    # 邮件发送回执信息,个人邮箱接收
    self_receivers = ['email1@163.com']
    self_cc = ['email2@163.com']
    self_sub = ''
    self_body = ''
    # 邮件发送失败回执信息,个人邮箱接收
    self_sub_fail = '**** 自动回复邮件失败 ****'
    self_body_fail = '自动回复邮件失败,请在收件箱查看并排查原因...'
    
    for account_name in accounts:
        # print(account_name.Name)
        # 获取当前用户下的所有文件夹,包括 收件箱,发件箱...
        level_1_names = account_name.Folders
        for level_1_name in level_1_names:
            # 获取当前系统时间
            cur_time = dt.datetime.now()
            # print('level_1_name ==', level_1_name.Name)
            if(level_1_name.Name in ['收件箱', '已发送邮件']):
                # 按接收时间由近到远排序
                cur_items = level_1_name.Items
                cur_items.Sort('[ReceivedTime]', True)
                # 收件箱邮件读取数量
                cur_count = 0
                for sigle_item in cur_items:
                    # print('sigle_item ==', sigle_item)
                    cur_count += 1
                    if(cur_count >=10):
                        break

                    # 发送人, 邮件主题, 接收时间, 收件人, 抄送人
                    cur_senderName = None
                    cur_subject = None
                    cur_received_time = None
                    cur_receiver = None
                    cur_cc = None
                    email_date = None
                    if(hasattr(sigle_item, 'SenderName')):
                        # print('****** sigle_item.SenderName ==', sigle_item.SenderName)
                        cur_senderName = sigle_item.SenderName
                    if(hasattr(sigle_item, 'Subject')):
                        cur_subject = sigle_item.Subject
                    if(hasattr(sigle_item, 'ReceivedTime')):
                        cur_received_time = sigle_item.ReceivedTime # 2021-12-17 11:37:40.095000+00:00 (cur_received_time 值样例)
                        # 邮件当前接收时间转换
                        email_date = date_format_trains(str(cur_received_time)) # 返回 (year, month, day)
                    if(hasattr(sigle_item, 'To')):
                        cur_receiver = sigle_item.To
                    if(hasattr(sigle_item, 'CC')):
                        cur_cc = sigle_item.CC
                    
                    # 发件箱已发送两次回复邮件时,退出整个脚本
                    if('已发送邮件' == level_1_name.Name):
                        # 查询发件箱已回复邮件次数
                        if('发件人邮箱' == cur_receiver and '抄送人名称' == cur_cc and '邮件主题' in cur_subject):
                            if(email_date == (cur_time.year, cur_time.month, cur_time.day)):
                                # 发送两次后,跳出整个循环
                                send_count += 1
                                if(send_count >= 2):
                                    print('it has sended 2 email, select_send_condition end...')
                                    return
                    elif('收件箱' == level_1_name.Name):
                        if(cur_senderName and cur_subject):
                            # 根据 发件人名称 以及 邮件主题 判断是否需要发送邮件
                            if('发件人名称' == cur_senderName and '收到邮件请立马回复，谢谢' in cur_subject):
                                cur_hour = cur_time.hour
                                cur_minute = cur_time.minute # type is int
                                print('email_date ==', email_date)
                                print('cur_time ==', (cur_time.year, cur_time.month, cur_time.day))
                                print('-'*50)
                                # 只发送当天需答复的邮件
                                if(email_date == (cur_time.year, cur_time.month, cur_time.day) and 1 >= (cur_hour-cur_received_time.hour)):
                                    # 随机 1-5 分钟发送回复邮件
                                    time.sleep(60*np.random.randint(1,6))
                                    # 自动回复邮件
                                    sendEmail(['email3@163.com'], cc, cur_subject, body)
                                    print('auto send email end...')
                                    # 休眠1分钟,避免触发ol并发异常
                                    time.sleep(60)
                                    # 回执邮件主题与内容
                                    self_sub = '邮件回执主题 - ' + str(cur_subject)
                                    self_body = '邮件发送成功,邮件收件人为: ' + ''.join(receivers) + ', 抄送人: ' + ''.join(cc) + ', 回复内容为: ' + body
                                    # 给个人邮件发送 邮件回执
                                    sendEmail(
                                        ['email1@163.com'], 
                                        ['email2@163.com'], 
                                        self_sub, 
                                        self_body
                                    )
                                    print('auto send self-email end...')
                                    # 回复一次邮件后,休眠2小时
                                    time.sleep(2*60*60)                
    print('select_send_condition end...')


if __name__ == '__main__':
    while True:
        try:
            # 上午9点开始执行,每2分钟执行一次
            if(9 <= dt.datetime.now().hour):
                cur_count += 1
                print(cur_count, '-- timer on...')
                receivers = ['email3@163.com'] # 实际中按发送邮件的发件人为准
                cc = ['email4@163.com']
                sub = '' # 实际中按发送邮件的主题为准
                body = '邮件主题内容'
                select_send_condition(
                    receivers, 
                    cc, 
                    sub, 
                    body
                )

                time.sleep(2*60)
                print(cur_count, '-- timer off...')
                
                # 在指定时间点后跳出脚本
                cur_timer = app_timer_out(18, 30)
                if(-1 == cur_timer):
                    break
        except Exception as e:
            # 发生异常时,给个人邮箱发送通知邮件
            sendEmail(
                ['email1@163.com'], 
                ['email2@163.com'], 
                '邮件发送发生错误', 
                '错误信息为: \n' + str(e)
            )
            print('send email failed, error ==', e)
            return